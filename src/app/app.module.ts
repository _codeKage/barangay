import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { EventPage } from '../pages/event/event';
import { SafetyTipPage } from '../pages/safety-tip/safety-tip';
import { PreparednessKitPage } from '../pages/preparedness-kit/preparedness-kit';
import { SearchEvacuationPage } from '../pages/search-evacuation/search-evacuation';
import { FactsAndDescriptionPage } from '../pages/facts-and-description/facts-and-description';
import { WhatToDoPage } from '../pages/what-to-do/what-to-do';
import {  GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { AdminHomePage } from '../pages/admin-home/admin-home';
import { HttpClientModule } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { AddHouseholdPage } from '../pages/add-household/add-household';
import { AddMemberPage } from '../pages/add-member/add-member';
import { AddSurveyPage } from '../pages/add-survey/add-survey';

@NgModule({
  declarations: [
    AddHouseholdPage,
    AddMemberPage,
    AddSurveyPage,
    AdminHomePage,
    EventPage,
    FactsAndDescriptionPage,
    MyApp,
    HomePage,
    LoginPage,
    PreparednessKitPage,
    SafetyTipPage,
    SearchEvacuationPage,
    WhatToDoPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AddHouseholdPage,
    AddMemberPage,
    AddSurveyPage,
    AdminHomePage,
    EventPage,
    FactsAndDescriptionPage,
    MyApp,
    HomePage,
    LoginPage,
    PreparednessKitPage,
    SafetyTipPage,
    SearchEvacuationPage,
    WhatToDoPage
  ],
  providers: [
    Geolocation,
    GoogleMaps,
    HttpClientModule,
    NativeStorage,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
