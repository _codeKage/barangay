import { Component, ViewChild } from '@angular/core';
import { AlertController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { EventPage } from '../pages/event/event';
import { SafetyTipPage } from '../pages/safety-tip/safety-tip';
import { PreparednessKitPage } from '../pages/preparedness-kit/preparedness-kit';
import { SearchEvacuationPage } from '../pages/search-evacuation/search-evacuation';
import { NativeStorage } from '@ionic-native/native-storage';
import { AdminHomePage } from '../pages/admin-home/admin-home';
import { AddSurveyPage } from '../pages/add-survey/add-survey';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private alertCtrl: AlertController, private storage: NativeStorage) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Events', component: EventPage },
      {title:'Safety Tip', component: SafetyTipPage},
      {title:'Preparedness Kit', component: PreparednessKitPage},
      {title:'Search Evacuation', component: SearchEvacuationPage},
      {title:'Logout', component: null },

    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.storage.getItem('user').then(user => {
        let userInstance = JSON.parse(user);
        this.rootPage = userInstance.usertype == 'Resident' ? HomePage : AdminHomePage;
      },() => {
        this.rootPage = LoginPage;
      })
    });
  }

  openPage(page) {
    if(page.component === null) {
      this.alertCtrl.create({
        title: 'Message',
        subTitle: 'Do you really want to logout?',
        buttons: [{
          text: 'No',
          role: 'cancel'
        }, {
          text: 'Yes',
          handler: () => {
            this.storage.remove('user').then(() => this.nav.setRoot(LoginPage));
          }
        }]
      }).present();
      return;
    }
    this.nav.setRoot(page.component);
  }
}
