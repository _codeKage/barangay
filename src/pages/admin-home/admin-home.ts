import { Component } from '@angular/core';
import { ActionSheetController, AlertController, IonicPage, LoadingController, NavController, NavParams, Refresher } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpClient } from '@angular/common/http';
import { AddHouseholdPage } from '../add-household/add-household';
import { AddMemberPage } from '../add-member/add-member';
import { AddSurveyPage } from '../add-survey/add-survey';

/**
 * Generated class for the AdminHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-home',
  templateUrl: 'admin-home.html',
})
export class AdminHomePage {

  houseHolds: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private storage: NativeStorage, private http: HttpClient, public actionSheet: ActionSheetController, private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
    this.initializeHouseholds();
  }

  logout(): void {
    this.alertCtrl.create({
      title: 'Message',
      subTitle: 'Are you sure you want to logout?',
      buttons: [{
        text: 'Yes',
       handler: () => {
          this.storage.remove('user').then(() => this.navCtrl.setRoot(LoginPage));
       }
      },
        {
          text: 'No',
          role: 'cancel'
        }]
    }).present();
  }


  initializeHouseholds(): void {
    let load = this.loadingCtrl.create({
      content: 'Loading'
    });
    load.present();
    this.storage.getItem('user').then(user => {
      let userInstance = JSON.parse(user);
      let postData = new FormData();
      postData.append('barangay_id', userInstance.barangay_about_id);
      this.http.post('http://disasterwebsite.com/jeremiah-api/load-barangay-households.php', postData).subscribe((data: any[]) => {
        this.houseHolds = [];
        data.filter((elem : any) => elem !== false).map((elem: string) => {
          this.houseHolds.push(JSON.parse(elem));
          return elem;
        });
        load.dismissAll();
      });
      load.dismissAll();
    });

  }

  openActionSheet(houseHold: any): void {
    this.actionSheet.create({
      title: 'What do you want to do?',
      buttons: [
        {
          text: 'View info',
          handler: () => {
            this.viewHouseholdInfo(houseHold);
          }
        }
        ,
        {
          text: 'Add Member',
          handler: () => {
              this.navCtrl.push(AddMemberPage, {
                houseHold: houseHold
              });
          }
        },
        {
          text: 'Add Survey',
          handler: () => {
            let postData = new FormData();
            postData.append('household', houseHold.id);

            this.http.post('http://disasterwebsite.com/jeremiah-api/check-yearly-survey',postData).subscribe(result => {
              if(+result > 0) {
                this.alertCtrl.create({
                  title: 'Message',
                  subTitle: 'Household already took survey',
                  buttons: ['ok']
                }).present();
              }else {
                this.navCtrl.push(AddSurveyPage, {
                  houseHold: houseHold
                });
              }
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    }).present();
  }

  refresh(event: Refresher): void {
    this.initializeHouseholds();
    event.complete();
  }

  private viewHouseholdInfo(houseHold: any) {
    let message: string = '';
    message += 'Purok: ' + houseHold.purok + '<br>';
    message += 'Street: ' + houseHold.street + '<br>';
    message += 'House Identification Number: ' + houseHold.house_identification_number + '<br>';
    message += 'Name of Respondent: ' + houseHold.name_of_respondent + '<br>';
    message += 'Contact: ' + houseHold.contact;
    this.alertCtrl.create({
      title: 'Household information',
      subTitle: message,
      buttons: ['Close']
    }).present();
  }

  openHouseHoldPage(): void {
    this.navCtrl.push(AddHouseholdPage);
  }

}
