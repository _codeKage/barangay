import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FactsAndDescriptionPage } from './facts-and-description';

@NgModule({
  declarations: [
    FactsAndDescriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(FactsAndDescriptionPage),
  ],
})
export class FactsAndDescriptionPageModule {}
