import { Component, ViewEncapsulation } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WhatToDoPage } from '../what-to-do/what-to-do';

/**
 * Generated class for the FactsAndDescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-facts-and-description',
  templateUrl: 'facts-and-description.html',
  encapsulation: ViewEncapsulation.None
})
export class FactsAndDescriptionPage {
  disaster: string;
  description: string;
  facts: string;
  hazard: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.hazard = JSON.parse(this.navParams.get('hazard'));
    this.disaster = this.hazard.hazard;
    this.description = this.hazard.description;
    this.facts = this.hazard.facts;
  }

  openNextPage(hazard: any): void {
    this.navCtrl.push(WhatToDoPage, {
      hazard: hazard
    });
  }

}
