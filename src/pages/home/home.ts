import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Barangay } from '../../models/barangay.model';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  barangay: Barangay = new Barangay();
  user: any;
  constructor(public navCtrl: NavController,
              private storage: NativeStorage,
              private http: HttpClient) {

  }

  ionViewDidLoad(): void {
    this.storage.getItem('user').then(user => {
      this.user = JSON.parse(user);
      this.initializeBarangay();
    });
  }

  private initializeBarangay(): void {
    let postData = new FormData();
    postData.append('barangay_id', this.user.barangay_about_id);
    this.http.post('http://disasterwebsite.com/jeremiah-api/load-barangay-info.php', postData).subscribe((data: any) => {
      this.barangay.name = data.barangay;
      this.barangay.mission = data.mission;
      this.barangay.vision = data.vision;
      this.barangay.logo = data.logo;
      console.log(this.barangay.logo)
    });
  }

}
