import { Component } from '@angular/core';
import { IonicPage, ToastController } from 'ionic-angular';

import { GoogleMap, GoogleMapOptions, GoogleMaps, ILatLng, Marker } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import {PolylineOptions} from '@ionic-native/google-maps';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpClient } from '@angular/common/http';

import * as vincenty from '../../../node_modules/node-vincenty';
import { Observable } from 'rxjs';

declare var google:any;
@IonicPage()
@Component({
  selector: 'page-search-evacuation',
  templateUrl: 'search-evacuation.html',
})
export class SearchEvacuationPage {

  map: GoogleMap;
  lng:number = 124.238022;
  lat:number = 13.584998;
  marker: Marker;
  destination: Marker;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  polyline = null;
  constructor(public geo: Geolocation, private toastCtrl: ToastController, private storage: NativeStorage, private http: HttpClient) {
  }

   ionViewDidLoad(): void {
    this.initialMapLoad();
    this.geo.watchPosition().subscribe((resp) => {
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
      this.marker.setPosition({
        lat: this.lat,
        lng: this.lng
      });
      this.displayDirection();
    }, () => {
      this.toastCtrl.create({
        message: 'Cannot get your location, please turn on your Internet Connectivity and GPS',
        duration: 3000,
      }).present();
    });
  }

  findNearestEvacuation(): Observable<any> {
      return Observable.create(observer => {
        let postData = new FormData();
        let center: any = undefined;
        let ctr=0;
        this.storage.getItem('user').then(user => {
          const userInstance = JSON.parse(user);
          postData.append('barangay_id', userInstance.barangay_about_id);
          this.http.post('http://disasterwebsite.com/jeremiah-api/load-barangay-evacuations.php',postData).subscribe((data: any[]) => {
            data.forEach(cntr => {
              if(ctr === 0) {
                center = cntr;
                ctr++;
              }
              if(vincenty.distVincenty(this.lat, this.lng, center.latitude, center.longitude).distance > vincenty.distVincenty(this.lat, this.lng, cntr.latitude, cntr.longitude).distance) {
                center = cntr;
              }
            });
            observer.next(center);
            observer.complete();
            return center;
          });
        });
      });
  }

  displayDirection() {
    this.directionsService.route({
      origin: this.marker.getPosition(),
      destination: this.destination.getPosition(),
      travelMode: google.maps.TravelMode.WALKING
    }, (response) => {

      this.directionsDisplay.setDirections(response);
      let legs = response.routes[0].legs;

      const points :ILatLng[] = legs.reduce((acc1, l) => {
        return [...acc1, ...l.steps.reduce((acc2, s) => {
          return [...acc2, ...s.path.map(p => ({lng: p.lng(), lat: p.lat()}))];
        },[])];
      }, []);
      const  polyLine :  PolylineOptions = {
        points: [...points],
        color: '#0000FF',
        geodesic: false
      };

      if (this.polyline != null) {
        this.polyline.remove();
        this.polyline = null;
      }
      this.map.addPolyline( polyLine ).then(polyline => this.polyline = polyline);

    });
  }

  validateLocation() : boolean {
    return typeof this.lng !== 'undefined' && typeof this.lat !== 'undefined';
  }

  initialMapLoad(): void {
    this.geo.getCurrentPosition().then(pos => {
      this.lng = pos.coords.longitude;
      this.lat = pos.coords.latitude;
      let mapOptions: GoogleMapOptions = {
        camera: {
          target: {
            lat: this.lat,
            lng: this.lng
          },
          zoom: 18,
          tilt: 30
        },
        mapType: 'MAP_TYPE_ROADMAP'
      };
      this.map = GoogleMaps.create('map_canvas', mapOptions);
      this.marker = this.map.addMarkerSync({
        title: 'Current Location',
        icon: 'blue',
        animation: 'DROP',
        position: {
          lat: this.lat,
          lng: this.lng
        }
      });
      this.marker.showInfoWindow();

      this.findNearestEvacuation().subscribe((nearestEvacuation: any) => {
        this.destination = this.map.addMarkerSync({
          title: 'Nearest Evacuation Center',
          icon: 'green',
          animation: 'DROP',
          position: {
            lat: nearestEvacuation.latitude,
            lng: nearestEvacuation.longitude
          }
        });
        this.destination.showInfoWindow();
        this.displayDirection();
      });
    });
  }
}
