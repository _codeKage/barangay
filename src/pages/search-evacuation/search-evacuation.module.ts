import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchEvacuationPage } from './search-evacuation';

@NgModule({
  declarations: [
    SearchEvacuationPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchEvacuationPage),
  ],
})
export class SearchEvacuationPageModule {}
