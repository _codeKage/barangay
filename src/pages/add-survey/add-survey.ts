import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { SurveyA } from '../../models/survey-a.model';
import { SurveyB } from '../../models/survey-b.model';
import { SurveyC } from '../../models/survey-c.model';
import { SurveyD } from '../../models/survey-d.model';
import { HttpClient } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the AddSurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-survey',
  templateUrl: 'add-survey.html',
})
export class AddSurveyPage {

  surveyA: SurveyA = new SurveyA();
  surveyB: SurveyB = new SurveyB();
  surveyC: SurveyC = new SurveyC();
  surveyD: SurveyD = new SurveyD();
  houseHoldId: number;
  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpClient, private storage: NativeStorage, private alert: AlertController) {
  }

  ionViewDidLoad(): void {
      this.houseHoldId = this.navParams.get('houseHold').id;
  }

  noSelected(value: string): void {
    if(value === 'No') {
      this.surveyA = new SurveyA();
    }
  }

  transformSurveyC(): void {
    this.surveyC.c4_1 = this.surveyC.c4_1 === true ? 'Yes' : 'No';
    this.surveyC.c4_2 = this.surveyC.c4_2 === true ? 'Yes' : 'No';
    this.surveyC.c4_3 = this.surveyC.c4_3 === true ? 'Yes' : 'No';
    this.surveyC.c4_4 = this.surveyC.c4_4 === true ? 'Yes' : 'No';
    this.surveyC.c4_5 = this.surveyC.c4_5 === true ? 'Yes' : 'No';
    this.surveyC.c4_6 = this.surveyC.c4_6 === true ? 'Yes' : 'No';
  }

  submitSurvey(): void {
    this.transformSurveyC();
    let user:any = null;
    let postData = new FormData();
    this.storage.getItem('user').then( (userInstance: any) => {
      user = JSON.parse(userInstance);
      postData.append('barangay_id', user.barangay_about_id);
      postData.append('surveyA', JSON.stringify(this.surveyA));
      postData.append('surveyB', JSON.stringify(this.surveyB));
      postData.append('surveyC', JSON.stringify(this.surveyC));
      postData.append('surveyD', JSON.stringify(this.surveyD));
      postData.append('household', this.houseHoldId.toString());
      this.http.post('http://disasterwebsite.com/jeremiah-api/create-barangay-survey.php',postData).subscribe(data => {
        console.log(data);
        this.alert.create({
          title: 'Message',
          subTitle: 'Survey Added Successfully',
          buttons: ['Ok']
        }).present();
        this.navCtrl.pop();
      });
    });
  }


}
