import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HttpClient } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { AdminHomePage } from '../admin-home/admin-home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username:string = '';
  password:string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpClient, private storage: NativeStorage, private toast: ToastController) {
  }

  ionViewDidLoad(): void {
  }

  login(): void {
    let postData = new FormData();
    postData.append('username', this.username);
    postData.append('password', this.password);
    console.log(this.username, this.password);
    this.http.post('http://disasterwebsite.com/jeremiah-api/login.php', postData).subscribe((data: any) => {
      if (data!==null) {
        this.storage.setItem('user', JSON.stringify(data));
        this.navCtrl.setRoot(data.usertype === 'Resident'? HomePage : AdminHomePage);
        return;
      }
      this.toast.create({
        message: 'Invalid Username and/or Password',
        duration: 3000
      }).present();
    },error => {
      console.log(error);
    } );
  }

}
