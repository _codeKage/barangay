import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FactsAndDescriptionPage } from '../facts-and-description/facts-and-description';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the SafetyTipPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-safety-tip',
  templateUrl: 'safety-tip.html',
})
export class SafetyTipPage {

  safetyTips: any[] = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: NativeStorage, 
    private http: HttpClient
  ) {
  }

  ionViewDidLoad() {
    this.initializeSafetyTips();
  }

  initializeSafetyTips() : void {
    this.storage.getItem('user').then(user => {
      const userInstance = JSON.parse(user);
      let postData = new FormData();
      postData.append('barangay_id', userInstance.barangay_about_id);
      this.http.post('http://disasterwebsite.com/jeremiah-api/load-barangay-hazards.php',postData)
      .subscribe((safetyTips: any[]) => {
        this.safetyTips = safetyTips;
      });
    })
  }

  openSecondPage(params: any): void {
    this.navCtrl.push(FactsAndDescriptionPage, {
      'hazard': JSON.stringify(params)
    });
  }

}
