import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SafetyTipPage } from './safety-tip';

@NgModule({
  declarations: [
    SafetyTipPage,
  ],
  imports: [
    IonicPageModule.forChild(SafetyTipPage),
  ],
})
export class SafetyTipPageModule {}
