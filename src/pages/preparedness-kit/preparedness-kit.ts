import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the PreparednessKitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-preparedness-kit',
  templateUrl: 'preparedness-kit.html',
})
export class PreparednessKitPage {
  preparednessKit: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: NativeStorage, private http: HttpClient) {
  }

  ionViewDidLoad() {
    this.initializePreparednessKit();
  }

  initializePreparednessKit(): void {
    this.storage.getItem('user').then(user => {
      const userInstance = JSON.parse(user);
      let postData = new FormData();
      postData.append('barangay_id', userInstance.barangay_about_id);
      this.http.post('http://disasterwebsite.com/jeremiah-api/load-barangay-kits.php', postData).subscribe((kit: any) => {
        this.preparednessKit = kit.kit;
      });
    });
  }

}
