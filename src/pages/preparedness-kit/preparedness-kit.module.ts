import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreparednessKitPage } from './preparedness-kit';

@NgModule({
  declarations: [
    PreparednessKitPage,
  ],
  imports: [
    IonicPageModule.forChild(PreparednessKitPage),
  ],
})
export class PreparednessKitPageModule {}
