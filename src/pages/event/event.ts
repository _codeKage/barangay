import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Event } from '../../models/event.model';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
/**
 * Generated class for the EventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage {

  events: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: NativeStorage, private http: HttpClient) {
  }

  ionViewDidLoad() {
    this.initializeEvents();
  }

  private initializeEvents(): void  {
    this.storage.getItem('user').then(user => {
      let userInstance = JSON.parse(user);
      let postData = new FormData();
      postData.append('barangay_id', userInstance.barangay_about_id);
      this.http.post('http://disasterwebsite.com/jeremiah-api/load-barangay-events', postData).subscribe((events : any[]) => {
        this.events = events.map((event: any) => {
          event.when = moment(event.when).format('ll');
          return event;
        });
        console.log(events);
      });
    });
  }

}
