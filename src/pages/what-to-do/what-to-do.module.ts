import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WhatToDoPage } from './what-to-do';

@NgModule({
  declarations: [
    WhatToDoPage,
  ],
  imports: [
    IonicPageModule.forChild(WhatToDoPage),
  ],
})
export class WhatToDoPageModule {}
