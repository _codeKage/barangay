import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the WhatToDoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-what-to-do',
  templateUrl: 'what-to-do.html',
})
export class WhatToDoPage {

  before: string;
  during: string;
  after: string;
  safetyTips: string;
  hazard: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.hazard = this.navParams.get('hazard');
    this.before = this.hazard.before;
    this.during = this.hazard.during;
    this.after = this.hazard.after;
    this.safetyTips = this.hazard.tips;
  }

}
