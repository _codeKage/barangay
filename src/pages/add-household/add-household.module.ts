import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddHouseholdPage } from './add-household';

@NgModule({
  declarations: [
    AddHouseholdPage,
  ],
  imports: [
    IonicPageModule.forChild(AddHouseholdPage),
  ],
})
export class AddHouseholdPageModule {}
