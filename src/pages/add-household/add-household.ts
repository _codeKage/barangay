import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpClient } from '@angular/common/http';
import { GoogleMap, GoogleMaps, Marker, GoogleMapsEvent } from '@ionic-native/google-maps';

/**
 * Generated class for the AddHouseholdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-household',
  templateUrl: 'add-household.html',
})
export class AddHouseholdPage {
  houseHold: HouseHold = new HouseHold();
  map: GoogleMap;
  marker: Marker;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: NativeStorage, private http: HttpClient) {
  }

  ionViewDidLoad() {
    this.initializePage();
  }

  initializePage(): void {
    this.storage.getItem('user').then(user => {
      let userInstance = JSON.parse(user);
      let postData = new FormData();
      postData.append('barangay_id', userInstance.barangay_about_id);
      this.http.post('http://disasterwebsite.com/jeremiah-api/load-barangay-info.php',postData).subscribe((data: any) => {
        this.houseHold.barangay_about_id = userInstance.barangay_about_id;
        this.houseHold.barangay = data.barangay;
        this.houseHold.municipality = data.municipality;
        this.houseHold.province = data.province;
        this.houseHold.latitude = data.latitude;
        this.houseHold.longitude = data.longitude;

        this.map = GoogleMaps.create('map_canvas', {
          mapType: 'MAP_TYPE_ROADMAP',
          camera : {
            target : {
              lat: +data.latitude,
              lng: +data.longitude
            },
            zoom: 17,
            tilt: 30,
          }
        });
        this.marker = this.map.addMarkerSync({
          title: 'Place House Location',
          icon: 'red',
          animation: 'DROP',
          position: {
            lat: +data.latitude,
            lng: +data.longitude
          },
          draggable: true
        });

        this.marker.on(GoogleMapsEvent.MARKER_DRAG_END).subscribe(() => {
          this.houseHold.latitude = this.marker.getPosition().lat;
          this.houseHold.longitude = this.marker.getPosition().lng;
        });
      });
    });
  }
  addHouseHold(): void {
    let postData = new FormData();
    postData.append('household', JSON.stringify(this.houseHold));
    this.http.post('http://disasterwebsite.com/jeremiah-api/create-barangay-household.php', postData).subscribe(res => {
      this.navCtrl.pop();
    });
  }

}

export class HouseHold {
  barangay_about_id: number = null;
  province: string = null;
  municipality: string = null;
  barangay: string = null;
  purok: string = null;
  street: string = null;
  house_identification_number: string = null;
  latitude: number = null;
  longitude: number = null;
  name_of_respondent: string = null;
  contact: string = null;
  no_family: number = null;
  no_members: number = null;
  disability: string = 'no';
  no_disability: number = 0;
  building_type: string = null;
  water_supply: string = null;
  toilet: string = null;
  water_filtration: string = 'yes';
  electricity: string = null;
  power_generator: string = null;
}
