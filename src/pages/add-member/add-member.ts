import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the AddMemberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-member',
  templateUrl: 'add-member.html',
})
export class AddMemberPage {

  houseHoldInstance: HouseholdMember = new HouseholdMember();

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: NativeStorage, private http: HttpClient) {
  }

  ionViewDidLoad() {
    this.houseHoldInstance.resident_household_id = this.navParams.get('houseHold').id;
    this.storage.getItem('user').then(data => {
      this.houseHoldInstance.barangay_about_id = JSON.parse(data).barangay_about_id;
      this.houseHoldInstance.family_belong = JSON.parse(data).no_family;
    });
  }

  addMember(): void {
    let postData = new FormData();
    postData.append('householdMember', JSON.stringify(this.houseHoldInstance));
    this.http.post('http://disasterwebsite.com/jeremiah-api/add-household-member.php', postData).subscribe(res => {
      console.log(res);
    });
  }

}

export class HouseholdMember {
  id?: number;
  barangay_about_id: number;
  resident_household_id: number;
  first_name: string;
  middle_name?: string = null;
  last_name: string;
  family_belong: number;
  relation: string;
  civil_status: string;
  gender: string;
  birthdate: string;
  age: number;
  same_address: string = 'Yes';
  attending_school: string;
  year_level: string = null;
  highest_level: string;
  pregnant: string = 'No';
  have_children: string = 'No';
  solo_parent: string = 'No';
  disability: string = 'No';
  disability_type: string = null;
  health_problem: string;
  employed: string;
  occupation: string;
  where_occupation : string;
}
