export class SurveyC {
  id?: number;
  c1: string = 'Yes';
  c2: string = 'No';
  c3: string = 'No';
  c4_1: any = false;
  c4_2: any = false;
  c4_3: any = false;
  c4_4: any = false;
  c4_5: any = false;
  c4_6: any = false;
  c5: string = 'No';
  c6: string = 'No';
}
