export class SurveyD {
  id?: number;
  d1: string = 'No';
  d2: string = 'No';
  d3: string = 'No';
  d4: string = 'No';
  d5: string = 'No';
  d6: string = 'No';
  d7: string = 'No';
  d8: string = 'No';
  d9: string = 'No';
  d10_1: boolean = false;
  d10_2: boolean = false;
  d10_3: boolean = false;
  d10_4: boolean = false;
  d10_5: boolean = false;
  d10_6: boolean = false;
  d10_7: boolean = false;
  d10_8: boolean = false;
  d10_9: boolean = false;
  d10_10: boolean = false;
  d10_11: boolean = false;
  d10_12: boolean = false;
  d10_13: boolean = false;
  d10_14: boolean = false;
}
