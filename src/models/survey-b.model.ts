export class SurveyB {
  id?: number;
  b1: string = 'No';
  b2_1: boolean = false;
  b2_2: boolean = false;
  b2_3: boolean = false;
  b2_4: boolean = false;
  b2_5: boolean = false;
  b2_6: boolean = false;
  b2_7: boolean = false;
  b2_8: boolean = false;
  b2_9: boolean = false;
  b2_10: boolean = false;
  b2_11: boolean = false;
  b2_12: boolean = false;
  b2_13: boolean = false;
  b2_14: boolean = false;
  b3_1: boolean = false;
  b3_2: boolean = false;
  b3_3: boolean = false;
  b3_4: boolean = false;
  b3_5: boolean = false;
  b3_6: boolean = false;
  b3_7: boolean = false;
  b3_8: boolean = false;
  b3_9: boolean = false;
  b3_10: boolean = false;
  b3_11: boolean = false;
  b3_12: boolean = false;
  b3_13: boolean = false;
  b3_14: boolean = false;
  b3_15: boolean = false;
  b4: string = 'No';
}
