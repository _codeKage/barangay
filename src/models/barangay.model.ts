export class Barangay {
  id: number;
  name: string;
  address?: string;
  vision: string;
  mission: string;
  logo: string;
}
